FROM debian:wheezy
MAINTAINER cocoon 


# install dependencies
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update
RUN apt-get -y install curl wget sqlite3

ENV DEBIAN_FRONTEND dialog


# install pyrun
RUN mkdir /opt/python
WORKDIR /opt/python

# get install-pyrun script
RUN wget --no-check-certificate  https://downloads.egenix.com/python/install-pyrun
RUN chmod +x install-pyrun 


ENV PLATFORM linux-x86_64
RUN ./install-pyrun --log --platform=linux-x86_64 --disable-certificate-checks ./

ENV PATH /opt/python/bin:$PATH


# install some python libs
ONBUILD ADD requirements.txt /tmp/
ONBUILD RUN pip install -r /tmp/requirements.txt

#WORKDIR /tmp
#RUN tar cvf pyrun.tar /opt/python


CMD /bin/bash
